/*
   This file is part of Thomas Fischer's clone of Konsole, an X terminal.

   Copyright 2012 by Thomas Fischer <fischer@unix-ag.uni-kl.de>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301  USA.
   */

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDateTime>

#include "Logger.h"
#include "ColorScheme.h"

namespace Konsole
{

Logger *Logger::m_singleInstance = 0L;

Logger *Logger::getInstance()
{
    if (m_singleInstance == 0L)
        m_singleInstance = new Logger();
    return m_singleInstance;
}

Logger::Logger()
    : m_curX(-1), m_curY(-1), m_lastTimeStepTimestamp(0), m_timeStep(0), m_bgColor(0), m_fgColor(0), m_queuedClearScreen(false), m_dirtyScreen(false), m_queuedScreenSizeWidth(-1), m_queuedScreenSizeHeight(-1), m_queueLastAddition(0)
{
    m_startTime = QDateTime::currentMSecsSinceEpoch();

    QFile *outputFile = new QFile("/tmp/konsole-log.txt");
    if (outputFile->open(QFile::WriteOnly)) {
        m_logOutput = new QTextStream(outputFile);
    } else
        Q_ASSERT_X(outputFile->isOpen(), "Logger::Logger", QString(QLatin1String("Could not open file \"%\" to write to")).arg(outputFile->fileName()).toAscii().data());

    QFile *jsonOutputFile = new QFile("/tmp/konsole-log.json");
    if (jsonOutputFile->open(QFile::WriteOnly)) {
        m_jsonOutput = new QTextStream(jsonOutputFile);
    } else
        Q_ASSERT_X(jsonOutputFile->isOpen(), "Logger::Logger", QString(QLatin1String("Could not open file \"%\" to write to")).arg(jsonOutputFile->fileName()).toAscii().data());
}

Logger::~Logger()
{
    flushTextQueue();

    m_logOutput->flush();
    m_jsonOutput->flush();
}

void Logger::logScreenSize(int w, int h)
{
    flushTextQueue();

    const QString msg = QString(QLatin1String("%1 screenSize(w=%2,h=%3)")).arg(timeString()).arg(w).arg(h);
    (*m_logOutput) << msg << endl;

    m_queuedScreenSizeWidth = w;
    m_queuedScreenSizeHeight = h;
}

void Logger::logNewPos(int x, int y)
{
    flushScreenOps("logNewPos");
    if (x != m_curX + 1 || m_curY != y)
        flushTextQueue();

    m_curX = x;
    m_curY = y;

    const QString msg = QString(QLatin1String("%1 newPos(x=%2,y=%3)")).arg(timeString()).arg(x).arg(y);
    (*m_logOutput) << msg << endl;
}

void Logger::logClearScreen()
{
    flushTextQueue();

    const QString msg = QString(QLatin1String("%1 clearScreen()")).arg(timeString());
    (*m_logOutput) << msg << endl;

    m_queuedClearScreen = true;
}

void Logger::logCharacter(int charCode)
{
    if (charCode < 32) return;

    flushScreenOps("logCharacter");

    const QChar c = QChar(charCode);

    queueChar(c);

    const QString msg = QString(QLatin1String("%1 char(code=%2=%3)")).arg(timeString()).arg(charCode).arg(c);
    (*m_logOutput) << msg << endl;
}

void Logger::logSpecial(SpecialEvent specialEvent)
{
    flushScreenOps("logSpecial");
    flushTextQueue();

    QString eventStr = QLatin1String("unknown");
    switch (specialEvent) {
    case  BackspaceEvent:
        eventStr = QLatin1String("Backspace"); break;
    case ClearScreenEvent:
        eventStr = QLatin1String("ClearScreen"); break;
    }

    const QString msg = QString(QLatin1String("%1 special(specialEvent=%2)")).arg(timeString()).arg(eventStr);
    (*m_logOutput) << msg << endl;
}

void Logger::logClearImage(int fromPos, int toPos, const QChar &fillWith)
{
    flushTextQueue();

    const QString msg = QString(QLatin1String("%1 clearImage(fromPos=%2,toPos=%3,fillWith=%4=%5)")).arg(timeString()).arg(fromPos).arg(toPos).arg(fillWith.unicode()).arg(fillWith);
    (*m_logOutput) << msg << endl;

    const QString json = QString(QLatin1String("{ \"time\": %1, \"command\": \"clearimage\", \"fromPos\": %2, \"toPos\": %3, \"fillWith\": \"%4\" }")).arg(m_timeStep).arg(fromPos).arg(toPos).arg(fillWith);
    (*m_jsonOutput) << json << endl;
}

void Logger::logScroll(ScrollDirection scrollDirection, int fromLine, int deltaY)
{
    const QString scrollName = scrollDirection == ScrollDown ? QLatin1String("Down") : QLatin1String("Up");

    flushScreenOps("logScroll");
    flushTextQueue();

    const QString msg = QString(QLatin1String("%1 scroll(scrollDirection=%2,fromLine=%3,deltaY=%4)")).arg(timeString()).arg(scrollName).arg(fromLine).arg(deltaY);
    (*m_logOutput) << msg << endl;

    const QString json = QString(QLatin1String("{ \"time\": %1, \"command\": \"scroll\", \"direction\": \"%2\", \"fromLine\": %3, \"deltaY\": %4 }")).arg(m_timeStep).arg(scrollName).arg(fromLine).arg(deltaY);
    (*m_jsonOutput) << json << endl;
}

void Logger::logSwitchScreen(int newScreen)
{
    flushScreenOps("logSwitchScreen");
    flushTextQueue();

    const QString msg = QString(QLatin1String("%1 switchScreen(newScreen=%2)")).arg(timeString()).arg(newScreen);
    (*m_logOutput) << msg << endl;

    const QString json = QString(QLatin1String("{ \"time\": %1, \"command\": \"switchscreen\", \"newScreen\": %2 }")).arg(m_timeStep).arg(newScreen);
    (*m_jsonOutput) << json << endl;
}

void Logger::logColor(ColorLayer colorLayer, int colorIndex)
{
    flushScreenOps("logColor");
    flushTextQueue();

    const QString msg = QString(QLatin1String("%1 color(layer=%2,index=%3)")).arg(timeString()).arg(colorLayer == BackgroundColor ? QLatin1String("Background") : QLatin1String("Foreground")).arg(colorIndex);
    (*m_logOutput) << msg << endl;

    if (colorLayer == ForegroundColor)
        m_fgColor = colorIndex;
}

void Logger::logMessage(const QString &text)
{
    const QString msg = QString(QLatin1String("%1 message(msg=%2)")).arg(timeString()).arg(text);
    (*m_logOutput) << msg << endl;
}

void Logger::flushTextQueue()
{
    const qint64 currTime = QDateTime::currentMSecsSinceEpoch();

    if (!m_queuedText.isEmpty()) {
        const QString msg = QString(QLatin1String("%1 queuedText(text=%2,x=%3,y=%4)")).arg(timeString()).arg(m_queuedText).arg(m_queueX).arg(m_queueY);
        (*m_logOutput) << msg << endl;

        if (currTime > m_lastTimeStepTimestamp + 1000)
            ++m_timeStep;
        m_lastTimeStepTimestamp = QDateTime::currentMSecsSinceEpoch();

        QString cleanedQueuedText = m_queuedText;
        cleanedQueuedText = cleanedQueuedText.replace(QChar('\\'), QLatin1String("\\\\")).replace(QChar('"'), QLatin1String("\\\""));
        if (!cleanedQueuedText.isEmpty()) {
            QString colorName = QLatin1String("#999");
            switch (m_fgColor) {
            case 0: colorName = QLatin1String("#999"); break;
            case 1: colorName = QLatin1String("#c000"); break;
            case 2: colorName = QLatin1String("#3f6"); break;
            case 3: colorName = QLatin1String("#fc3"); break;
            case 4: colorName = QLatin1String("#36f"); break;
            case 5: colorName = QLatin1String("#f39"); break;
            default: colorName = QLatin1String("#09f"); break;
            }

            const QString json = QString(QLatin1String("{ \"time\": %1, \"command\": \"text\", \"text\": \"%2\", \"posX\": %3, \"posY\": %4, \"fgColor\": \"%5\" }")).arg(m_timeStep).arg(cleanedQueuedText).arg(m_queueX).arg(m_queueY).arg(colorName);
            (*m_jsonOutput) << json << endl;
            m_dirtyScreen = true;
        }

        m_queuedText = QString::null;
        m_queueLastAddition = 0;
    }
}

void  Logger::flushScreenOps(const QString &caller)
{
    if (m_queuedClearScreen && m_dirtyScreen) {
        const QString json = QString(QLatin1String("{ \"time\": %1, \"command\": \"clearscreen\", \"caller\": \"%2\" }")).arg(m_timeStep).arg(caller);
        (*m_jsonOutput) << json << endl;
    }

    if (m_queuedScreenSizeWidth > 0 && m_queuedScreenSizeHeight > 0) {
        const QString json = QString(QLatin1String("{ \"time\": %1, \"command\": \"screensize\", \"width\": %2, \"height\": \"%3\", \"caller\": \"%4\" }")).arg(m_timeStep).arg(m_queuedScreenSizeWidth).arg(m_queuedScreenSizeHeight).arg(caller);
        (*m_jsonOutput) << json << endl;
        m_queuedScreenSizeWidth = m_queuedScreenSizeHeight = -1;
    }

    m_queuedClearScreen = m_dirtyScreen = false;
}

void Logger::queueChar(const QChar &c)
{
    const qint64 currTime = QDateTime::currentMSecsSinceEpoch();

    if (currTime > m_queueLastAddition + 500)
        flushTextQueue();

    if (m_queuedText.isEmpty()) {
        m_queueX = m_curX;
        m_queueY = m_curY;
        m_queuedText = c;
    } else
        m_queuedText.append(c);

    m_queueLastAddition = currTime;
}

QString Logger::timeString() const
{
    return QString(QLatin1String("%1")).arg(QDateTime::currentMSecsSinceEpoch() - m_startTime, 10, 10, QChar('0'));
}
}

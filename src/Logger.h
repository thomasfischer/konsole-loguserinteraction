/*
   This file is part of Thomas Fischer's clone of Konsole, an X terminal.

   Copyright 2012 by Thomas Fischer <fischer@unix-ag.uni-kl.de>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301  USA.
   */

#ifndef LOGGER_H
#define LOGGER_H

#include <QString>

class QTextStream;

namespace Konsole
{
class Logger
{
public:
    enum ScrollDirection {ScrollUp, ScrollDown};
    enum ColorLayer {BackgroundColor, ForegroundColor};
    enum SpecialEvent {BackspaceEvent, ClearScreenEvent};

    static Logger *getInstance();
    ~Logger();

    void logScreenSize(int w, int h);
    void logNewPos(int x, int y);
    void logClearScreen();
    void logCharacter(int charCode);
    void logSpecial(SpecialEvent specialEvent);
    void logSwitchScreen(int newScreen);
    void logClearImage(int fromPos, int toPos, const QChar &fillWith);
    void logScroll(ScrollDirection scrollDirection, int fromLine, int deltaY);
    void logColor(ColorLayer colorLayer, int colorIndex);
    void logMessage(const QString &msg);

protected:
    Logger();

private:
    static Logger *m_singleInstance;
    qint64 m_startTime;
    int m_curX, m_curY;

    QTextStream *m_logOutput, *m_jsonOutput;

    qint64 m_lastTimeStepTimestamp;
    int m_timeStep;

    int m_bgColor, m_fgColor;

    bool m_queuedClearScreen, m_dirtyScreen;
    int m_queuedScreenSizeWidth, m_queuedScreenSizeHeight;
    void flushScreenOps(const QString &caller = QString::null);

    QString m_queuedText;
    int m_queueX, m_queueY;
    qint64 m_queueLastAddition;
    void flushTextQueue();
    void queueChar(const QChar &c);

    inline QString timeString() const;
};
}

#endif // LOGGER_H
